// Bài 1

function sapXep(idNum1,idNum2,idNum3,idClick,idResual){
    let number1 = document.querySelector(idNum1);
    let number2 = document.querySelector(idNum2);
    let number3 = document.querySelector(idNum3);
    let click = document.querySelector(idClick);
    let resual = document.querySelector(idResual);

     click.addEventListener("click",function(){
            let num1 = parseInt(number1.value)
            let num2 = parseInt(number2.value)
            let num3 = parseInt(number3.value)
        
            if(num1 < num2 && num2 < num3){
                resual.innerHTML = `<span>Số thứ tự tăng dần là: ${num1},${num2},${num3}</span>`
            }else if(num1<num3 && num3 < num2){
                resual.innerHTML = `<span>Số thứ tự tăng dần là: ${num1},${num3},${num2}</span>`
            }else if(num2 < num1 && num1 < num3 ){
                resual.innerHTML = `<span>Số thứ tự tăng dần là: ${num2},${num1},${num3}</span>`
            }else if(num2 < num3 && num3 < num1){
                resual.innerHTML = `<span>Số thứ tự tăng dần là: ${num2},${num3},${num1}</span>`
            }else if(num3< num2 && num2 <num1){
                resual.innerHTML = `<span>Số thứ tự tăng dần là: ${num3},${num2},${num1}</span>`
            }else if(num3<num1 && num1 <num2){
                resual.innerHTML = `<span>Số thứ tự tăng dần là: ${num3},${num1},${num2}</span>`
            }else{
                resual.innerHTML = `<span>Các số bằng nhau: ${num1},${num2},${num3}</span>`
            }
        })
}

sapXep(".number1",".number2",".number3",".click",".resual span");



// Bài 2

function chaoHoi(idMember,idClick,idgreeting){
    let member = document.getElementById(idMember);
    let click = document.querySelector(idClick);
    let greeting = document.querySelector(idgreeting);
    click.addEventListener("click",function(){
        switch(member.value){
            case "Bo":{
                greeting.innerHTML = `<span>Xin chào Bố!</span>`
                breack;
            }
            case "Me": {
                greeting.innerHTML = `<span>Xin chào Mẹ!</span>`
                breack;
            }
            case "Anh":{
                greeting.innerHTML = `<span>Xin chào Anh Trai!</span>`
                breack;
            }
            case "Em":{
                greeting.innerHTML = `<span>Xin chào Em Gái!</span>`
                breack;
            }
        }
    })
}
chaoHoi("exampleFormControlSelect1",".clickGreeting",".greeting span")

// Bài 3

function demSo(idNum1, idNum2, idNum3, idClick, idResual){
    let num1 = document.querySelector(idNum1);
    let num2 = document.querySelector(idNum2);
    let num3 = document.querySelector(idNum3);
    let click = document.querySelector(idClick);
    let resual = document.querySelector(idResual);
    click.addEventListener("click",function(){
        let number1 = parseInt(num1.value)
        let number2 = parseInt(num2.value)
        let number3 = parseInt(num3.value)

        if (number1 % 2 == 0 && number2 % 2 == 0 && number3 % 2 == 0){
            let soChan = 3;
            resual.innerHTML= `<span>Có ${3 - soChan} số Lẽ,  ${soChan} số chẵn</span>`
        }else if (number1 % 2 != 0 && number2 % 2 == 0 && number3 % 2 == 0 || number1 % 2 == 0 && number2 % 2 != 0 && number3 % 2 == 0 || number1 % 2 == 0 && number2 % 2 == 0 && number3 % 2 != 0){
            let soChan = 2;
            resual.innerHTML= `<span>Có ${3 - soChan} số Lẽ,  ${soChan} số chẵn</span>`
        }else if(number1 % 2 != 0 && number2 % 2 != 0 && number3 % 2 == 0 || number1 % 2 == 0 && number2 % 2 != 0 && number3 % 2 != 0 || number1 % 2 != 0 && number2 % 2 == 0 && number3 % 2 != 0){
            let soChan = 1;
            resual.innerHTML= `<span>Có ${3 - soChan} số Lẽ,  ${soChan} số chẵn</span>`
        }else{
            let soChan = 0;
            resual.innerHTML= `<span>Có ${3 - soChan} số Lẽ,  ${soChan} số chẵn</span>`
        }
    })  
}
demSo(".num1", ".num2" , ".num3", ".click3" , ".resual3 span")


// Bai 4

function tamGiac(idCanh1, idCanh2, idCanh3, idDuDoan, idKetQua){
    let doDaiCanh1 = document.querySelector(idCanh1);
    let doDaiCanh2 = document.querySelector(idCanh2);
    let doDaiCanh3 = document.querySelector(idCanh3);
    let duDoan = document.querySelector(idDuDoan);
    let ketQua = document.querySelector(idKetQua);

    duDoan.addEventListener("click", function(){
        let canh1 = parseInt(doDaiCanh1.value);
        let canh2 = parseInt(doDaiCanh2.value);
        let canh3 = parseInt(doDaiCanh3.value);
        if(canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh2 + canh3 > canh1){
            if(canh1 == canh2 && canh1 == canh3 && canh2 == canh3){
                ketQua.innerHTML = `<span>Hình tam giác đều</span>`
            }else if(canh1 == canh2 || canh1 == canh3 || canh2 == canh3){
                ketQua.innerHTML = `<span>Hình tam giác cân</span>`
            }else if((canh1*canh1) + (canh2*canh2) == (canh3*canh3) || (canh1*canh1) + (canh3*canh3) == (canh2*canh2) || (canh2*canh2) + (canh3*canh3) == (canh1*canh1)){
                ketQua.innerHTML = `<span>Hình tam giác Vuông</span>`
            }else{
                ketQua.innerHTML = `<span>Một loại tam giác Khác</span>`
            }
        }else(
            alert("Dữ liệu không hợp lệ")
        )
    })
}

tamGiac(".canh1",".canh2",".canh3",".duDoan",".ketQua span")

// bài 5

function tinhNgayThangNam(idNgay,idThang,idNam,idHomQua,idNgayMai,idketQuaTinhNgay){
    let soNgay = document.querySelector(idNgay);
    let soThang = document.querySelector(idThang);
    let soNam = document.querySelector(idNam);
    let homQua = document.querySelector(idHomQua);
    let ngayMai = document.querySelector(idNgayMai);
    let ketQuarTinhNgay = document.querySelector(idketQuaTinhNgay);

    homQua.addEventListener("click",function(){
        let ngay = parseInt(soNgay.value);
        let thang = parseInt(soThang.value);
        let nam = parseInt(soNam.value);
        
        let namNhuan = nam % 4 === 0 && nam % 100 !== 0 || nam % 400 === 0;
        let thang30Ngay = thang === 5 || thang === 7 || thang === 10 || thang === 12;
        let thang31Ngay = thang === 1 || thang === 2 || thang === 4 || thang === 6 || thang ==8 || thang === 9 || thang === 11;

        if( namNhuan && thang === 2 && ngay <= 29 && ngay > 1){
            ketQuarTinhNgay.innerHTML = `<span>${ngay - 1}/${thang}/${nam}</span>`
        }else if( namNhuan && ngay === 1 && thang === 3){
            ketQuarTinhNgay.innerHTML = `<span>${ngay = 29 }/${thang - 1}/${nam}</span>`
        }else if(ngay === 1 && thang === 3){
            ketQuarTinhNgay.innerHTML = `<span>${ngay = 28 }/${thang - 1}/${nam}</span>`
        } else if (ngay === 1 && thang30Ngay){
            ketQuarTinhNgay.innerHTML = `<span>${ngay = 30 }/${thang - 1}/${nam}</span>`
        }else if (ngay === 1 && thang31Ngay && thang !== 1){
            ketQuarTinhNgay.innerHTML = `<span>${ngay = 31 }/${thang - 1}/${nam}</span>`
        }else if (ngay === 1 && thang === 1 && nam){
            ketQuarTinhNgay.innerHTML = `<span>${ngay = 31 }/${thang}/${nam - 1}</span>`
        } else if(ngay >= 30 && thang ===2 && namNhuan){
            alert("Dữ liệu không hợp lệ")
        } else if ( ngay > 28 && thang === 2){
            alert("Dữ liệu không hợp lệ")
        }else if (ngay > 0 && ngay < 32 && thang < 13 && nam > 1000){
            ketQuarTinhNgay.innerHTML = `<span>${ngay -1}/${thang}/${nam}</span>`
        }else{
            alert("Dữ liệu không hợp lệ")
        }
    })
    ngayMai.addEventListener("click",function(){
        let ngay = parseInt(soNgay.value);
        let thang = parseInt(soThang.value);
        let nam = parseInt(soNam.value);
        
        let namNhuan = nam % 4 === 0 && nam % 100 !== 0 || nam % 400 === 0;
        let thang30Ngay = thang === 4 || thang === 6 || thang === 9 || thang === 11;
        let thang31Ngay = thang === 1 || thang === 3 || thang === 5 || thang === 7 || thang ==8 || thang === 10 || thang === 12;

        if(ngay === 31 && thang31Ngay && thang !==12 || ngay === 30 && thang30Ngay || thang === 2 && namNhuan && ngay === 29 || ngay == 28 && thang === 2 && !namNhuan ){
            ketQuarTinhNgay.innerHTML = `<span>${ngay = 1}/${thang + 1}/${nam}</span>`
        }else if (ngay === 31 && thang === 12 ){
            ketQuarTinhNgay.innerHTML = `<span>${ngay = 1}/${thang = 1}/${nam + 1}</span>`
        }else if (ngay >= 30 && thang ===2 && namNhuan){
            alert("Dữ liệu không hợp lệ")
        }else if( ngay > 28 && thang === 2){
            alert("Dữ liệu không hợp lệ")
        }else if(ngay > 0 && ngay < 32 && thang > 0 && thang < 13 && nam > 1000){
            ketQuarTinhNgay.innerHTML = `<span>${ngay + 1}/${thang}/${nam}</span>`
        }else{
            alert("Dữ liệu không hợp lệ")
        }
    })
}

tinhNgayThangNam(".ngay",".thang",".nam",".homQua",".ngayMai",".ketQuaTinhNgay span")

// bai 6

function tinhNgay(idThang , idNam, idClick, idResual){
    let soThang = document.querySelector(idThang);
    let soNam = document.querySelector(idNam);
    let click = document.querySelector(idClick);
    let resual = document.querySelector(idResual);

    click.addEventListener("click",function(){
        let thang = parseInt(soThang.value);
        let nam = parseInt(soNam.value)
        
        let namNhuan = nam % 4 === 0 && nam % 100 !== 0 || nam % 400 === 0;
        let thang31Ngay = thang === 1 || thang === 3 || thang === 5 || thang === 7 || thang ==8 || thang === 10 || thang === 12;
        let thang30Ngay = thang === 4 || thang === 6 || thang === 9 || thang === 11;
        if(thang === 2 && namNhuan){
            resual.innerHTML = `<span>Tháng ${thang} Năm ${nam} có 29 ngày</span>`
        }else if(thang === 2 && !namNhuan){
            resual.innerHTML = `<span>Tháng ${thang} Năm ${nam} có 28 ngày</span>`
        }else if (thang31Ngay && thang>0 && thang<=12 && nam > 1000){
            resual.innerHTML = `<span>Tháng ${thang} Năm ${nam} có 31 ngày</span>`
        }else if(thang30Ngay && thang>0 && thang<=12 && nam > 1000){
            resual.innerHTML = `<span>Tháng ${thang} Năm ${nam} có 30 ngày</span>`
        }else {
            alert("Dữ liệu không hợp lệ")
        }
    })
}

tinhNgay(".ex--thang",".ex--nam",".ex--tinhNgay",".ex-ketQuaTinhNgay span")

// bai 7

function docSo(idSoNguyen, idClick, idResual1,idResual2,idResual3){
    let soNguyen = document.querySelector(idSoNguyen);
    let click = document.querySelector(idClick);
    let resual1 = document.querySelector(idResual1);
    let resual2 = document.querySelector(idResual2);
    let resual3 = document.querySelector(idResual3);
    click.addEventListener("click",function(){
        let so = parseInt(soNguyen.value)
        
        let hangTram = Math.floor(so /100);
        let hangChuc = Math.floor(so % 100 / 10);
        let donVi = so % 10;

        if(so > 99 && so < 1000){
            if(hangTram == 1){
                resual1.innerHTML = `Một trăm`
            }else if(hangTram == 2){
                resual1.innerHTML = `Hai trăm`
            }else if(hangTram == 3){
                resual1.innerHTML = `Ba trăm`
            }else if(hangTram == 4){
                resual1.innerHTML = `Bốn trăm`
            }else if(hangTram == 5){
                resual1.innerHTML = `Năm trăm`
            }else if(hangTram == 6){
                resual1.innerHTML = `Sau trăm`
            }else if(hangTram == 7){
                resual1.innerHTML = `Bảy trăm`
            }else if(hangTram == 8){
                resual1.innerHTML = `Tắm trăm`
            }else if(hangTram == 9){
                resual1.innerHTML = `Chín trăm`
            }
    
            if(hangChuc == 0){
                resual2.innerHTML = `<span>lẻ</span>`
            }else if(hangChuc == 1){
                resual2.innerHTML = `<span>mười</span>`
            }else if(hangChuc == 2){
                resual2.innerHTML = `<span>hai mươi</span>`
            }else if(hangChuc == 3){
                resual2.innerHTML = `<span>ba mươi</span>`
            }else if(hangChuc == 4){
                resual2.innerHTML = `<span>bốn mươi</span>`
            }else if(hangChuc == 5){
                resual2.innerHTML = `<span>năm mươi</span>`
            }else if(hangChuc == 6){
                resual2.innerHTML = `<span>sáu mươi</span>`
            }else if(hangChuc == 7){
                resual2.innerHTML = `<span>bảy mươi</span>`
            }else if(hangChuc == 8){
                resual2.innerHTML = `<span>tám mươi</span>`
            }else if(hangChuc == 9){
                resual2.innerHTML = `<span>chín mươi</span>`
            }
    
            if(donVi == 0){
                resual3.innerHTML = `<span> </span>`
            }else if(donVi == 1){
                resual3.innerHTML = `<span>một</span>`
            }else if(donVi == 2){
                resual3.innerHTML = `<span>hai</span>`
            }else if(donVi == 3){
                resual3.innerHTML = `<span>ba</span>`
            }else if(donVi == 4){
                resual3.innerHTML = `<span>tư</span>`
            }else if(donVi == 5){
                resual3.innerHTML = `<span>lăm</span>`
            }else if(donVi == 6){
                resual3.innerHTML = `<span>sáu</span>`
            }else if(donVi == 7){
                resual3.innerHTML = `<span>bảy</span>`
            }else if(donVi == 8){
                resual3.innerHTML = `<span>tám</span>`
            }else if(donVi == 9){
                resual3.innerHTML = `<span>chín</span>`
            }
        }else{
            alert("Vui lòng nhập số nguyên có 3 chữ số")
        }
    })
}

docSo(".ex--soNguyen", ".ex--docSo" , ".ex-ketQuaDocso .resual1", ".ex-ketQuaDocso .resual2", ".ex-ketQuaDocso .resual3")